# WailsTodos

[Wails](https://wails.app/) is a framework to build native cross-platform desktop application from web technologies for the frontend and [Go](https://golang.org/) for the backend, all packaged in one executable binary.

This repository is the result of following the third **Wails** tutorial, itself based on the **Todos MVC** [VueJS](https://vuejs.org/) tutorial.

## Quick test

* Install **Wails** after ensuring you have prerequisites.
* Clone this repository
* Build the app

```sh
$ wails build -d
```
The `-d` flag permits to have log messages in the shell console, and devtools enabled in the embedded browser.
